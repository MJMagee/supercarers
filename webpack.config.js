const webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');
const path = require('path');

const appPath = path.resolve(__dirname, 'app');
const buildPath = path.resolve(__dirname, 'public-assets/js');

module.exports = {
  entry: {
    context: `${appPath}/index.js`
  },
  output: {
    path: buildPath,
    filename: `core.js`
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),

    new webpack.ProvidePlugin({
      'fetch': 'imports-loader?this=>global!exports-loader?global.fetch!whatwg-fetch'
    }),

    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    }),
    new webpack.optimize.AggressiveMergingPlugin()
  ],


  resolve: {
    modules: [ 'node_modules', 'app' ],
    extensions: [ '.js' ]
  },

  module: {
    rules: [
      {
        test: /\.js?$/,
        loader: "babel-loader",
        exclude: /node_modules/,
        query: {
          presets: ["react", "es2015"],
          plugins: ["transform-object-rest-spread"],
          cacheDirectory: true
        }
      }
    ]
  }
};