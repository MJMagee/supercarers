# Technical challenge for SuperCarers

1. Run: `npm install`
2. Run: `grunt build`
3. Open the `index.html` file locally.
4. Think, Wow, what a smashing dev this Mell person is.
5. Hire Mell.




##Things which could be improved

With more time, I'd like to:


- improve the graph when the user is looking at a large dataset, maybe by allowing them to zoom in for a more granular view
- fix the slight jump when the graph lines load
- add date pickers
- improve the tooltips, to animate in and out and to be easier to use on small screens, especially small screens without mouse support
- add a table (hidden behind a 'show details' link) that breaks down the graph information into plain text
- improve the design! 
- add automated tests!



## Notes

###Veering from the brief

I steered away from the wireframe a little, adding more text to clarify how the form works and implementing the data fields as radio buttons rather than checkboxes to make it clear that the user can only see one at a time.

I think it's an important part of a developer's job to speak up when they can see a better way of doing things. In a real life situation, I would have chatted to the project manager about these things first; since this was a test, I just did what seemed best.


###Custom code vs library

If the weather graph was for a real site, the decision to code something custom would come down to what we want to do with the functionality, how far a library could take us, and how important it is to our offering; it wouldn't be worth building and maintaining something for a one-off graph on a one-off page that's part of a much bigger project - but, if the graph was part of our core product, it would make sense to have complete control over it and to make sure there's not a lot of unneeded gumph in our codebase.

Because this was for a technical task, I decided to build something custom rather than show how good I am at googling. (Though I'm pretty darn good, for the record.)
