import 'babel-polyfill';
import './scripts/cssua';
import './scripts/modernizr';

import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import reducer from './reducers'

import {DefaultLayout as Layout} from './views/layouts';
import Weather from './views/weather';


const middleware = [ thunk ];
const store = createStore(
  reducer,
  applyMiddleware(...middleware)
);

const App = (
  <Provider store={store}>
    <Layout>
      <Weather />
    </Layout>
  </Provider>
);


ReactDOM.render(App, document.getElementById('container'));
