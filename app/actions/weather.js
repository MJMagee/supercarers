export const weatherChangeDate = (type, date) => {
  switch (type) {

    case 'start':
      return {
        type: 'CHANGE_START_DATE',
        startDate: date
      };

    default:
      return {
        type: 'CHANGE_END_DATE',
        endDate: date
      };
      break;
  }
};


export const weatherChangeDataType = (data) => {
  return {
    type: 'CHANGE_DATA_TYPE',
    dataToShow: data
  }
};