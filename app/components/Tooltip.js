import React, { Component } from 'react';
import cl from 'classnames';

class Tooltip extends Component {

  constructor() {
    super();

    this.state = {
      isOpen: false
    };

    this.openTooltip = this.openTooltip.bind(this);
    this.closeTooltip = this.closeTooltip.bind(this);
  }

  openTooltip() {
    this.setState({ isOpen: true });
  }

  closeTooltip() {
    this.setState({ isOpen: false });
  }

  render() {
    const { el = 'div', className, tooltip, children } = this.props;
    const { isOpen } = this.state;

    return React.createElement(
      el,
      {
        className: cl(className, 'has-tooltip', { 'open': isOpen }),
        onMouseEnter: this.openTooltip,
        onMouseLeave: this.closeTooltip
      },
      <div>
        <div className="tooltip">
          { tooltip }
        </div>
        { children }
      </div>
    );
  }
}

export default Tooltip;



