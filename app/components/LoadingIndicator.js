import React, { Component } from 'react';
import i18n from 'i18n';

class LoadingIndicator extends Component {
  render() {
    return (
      <p className="loading">
        <span className="hidden">{ i18n.putOut('Loading') }</span>
      </p>
    );
  }
}

export default LoadingIndicator;
