import React, {Component} from 'react';
import cl from 'classnames';
import i18n from 'i18n';

import Tooltip from 'components/Tooltip';
import { getRealValue, getPercentage } from './functions';

const tooltipCutOff = 60;
const thickStylingCutOff = 48;

class GraphPoints extends Component {
  render() {
    const {
      data,
      dataToShow,
      id,
      date,
      value,
      highestValue,
      lowestValue,
      graphWidth,
      graphHeight
    } = this.props;
    const noOfDates = data.length;
    const liWidth = graphWidth / noOfDates;


    // Get the current and next point positions for all but the last point
      let lineLength = false;
      let degrees = false;

      if (id + 1 < noOfDates && data[id + 1]) {

        const currentPointPositionX = id * liWidth;
        const currentPointPositionY = getRealValue(getPercentage(value, highestValue, lowestValue), graphHeight);

        const nextPointPositionX = (id + 1) * liWidth;
        const nextValue = data[id + 1]['value'];

        if (typeof nextValue !== 'undefined') {
          const nextPointPositionY = getRealValue(getPercentage(nextValue, highestValue, lowestValue), graphHeight);

          // MATHS!
          // The position between points is a triangle that we know the opposite (the next point) and the
          // adjacent (the horizontal line between the current point and where it meets the vertical line for the
          // next point) for. With that information, we can get the arctan (which comes back in radians) and
          // translate it to degrees, for `transform` to use.
          // This gives us the correct angle, but the transformed line is a little too short to meet the next
          // point - we need to use good ol' Pythagoras' Theorem to get what the line should be (the square root
          // of the opposite squared plus the adjacent squared).
            const adjLength = nextPointPositionX - currentPointPositionX;
            const oppLength = nextPointPositionY - currentPointPositionY;
            degrees = -(Math.atan(oppLength / adjLength) * (180 / Math.PI));
            lineLength = Math.sqrt(Math.pow(oppLength, 2) + Math.pow(adjLength, 2));
        }
      }

    // Get position of each element
      const pointPosition = getPercentage(value, highestValue, lowestValue);


    // Render the graph points
      let valueLabel = '';
      let suffix = '';
      switch (dataToShow) {
        case 'tmax_C':
          valueLabel = 'Max. temperature:';
          suffix = 'c';
          break;

        case 'tmin_C':
          valueLabel = 'Min. temperature:';
          suffix = 'c';
          break;

        case 'rain_mm':
          valueLabel = 'Rain fall:';
          suffix = 'mm';
          break;

        default:
          valueLabel = 'Sunshine hours:';
          break;
      }

      return (
        <div className="point-position" style={{ bottom: `${pointPosition}%` }}>

          { noOfDates <= tooltipCutOff ?
            <Tooltip
              className="point-wrapper"
              tooltip={[
                <p className="title">{ date }</p>,
                <p><span dangerouslySetInnerHTML={{ __html: i18n.putOut(valueLabel) }} /> <strong>{ value }{ suffix }</strong></p>
              ]}>
              <span className="point" />
            </Tooltip>
          : null }

          {lineLength ? (
            <span className={ cl('line', { 'thin': noOfDates > thickStylingCutOff }) } style={{width: `${lineLength}px`, transform: `rotate(${degrees}deg)`}} />
          ) : null}
        </div>
      );
  }
}

export default GraphPoints;
