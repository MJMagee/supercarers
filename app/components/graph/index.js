import React, {Component} from 'react';
import cl from 'classnames';
import moment from 'moment';
import i18n from 'i18n';

import { getPercentage, roundUpToNearest10, roundDownToNearest10 } from './functions';
import GraphPoints from './GraphPoints';


class GraphWrapper extends Component {
  constructor() {
    super();

    this.state = {
      graphWidth: 500,
      graphHeight: 350
    };

    this.updateGraphSizes = this.updateGraphSizes.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      this.updateGraphSizes();
    }, 500);
    window.addEventListener('resize', this.updateGraphSizes);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateGraphSizes);
  }

  updateGraphSizes() {
    const graph = this.graph;

    if (graph && graph.childNodes[0]) {
      this.setState({
        graphWidth: graph.offsetWidth,
        graphHeight: graph.childNodes[0].offsetHeight // li:first-child height
      });
    }
  }


  render() {
    let { data, startDate, endDate, dataToShow } = this.props;
    const { graphWidth, graphHeight } = this.state;

    const dateFormat = 'MM/YYYY';
    startDate = moment(startDate, dateFormat);
    endDate = moment(endDate, dateFormat);


    // If we don't have the data, return
      if (!data || !startDate || !endDate || !dataToShow) { return null; }


    // Get number of years between the given dates
      const noOfYears = endDate.diff(startDate, 'years');


    // Get information to put out
      let filteredData = [];
      let highestValue = 0;
      let lowestValue = 0;
      data.forEach((v) => {
        const date = `${v.month}/${v.yyyy}`;
        const value = parseFloat(v[dataToShow]);
        if (!moment(date, dateFormat).isBetween(startDate, endDate, null, '[]') || !value) { return false; }

        if (value > highestValue) { highestValue = value; }
        if (value < lowestValue) { lowestValue = value; }

        filteredData.push({ date, value });
      });


    // Add a little padding
      const graphPadding = ['tmax_C', 'tmin_C'].includes(dataToShow) ? 2 : 10;
      highestValue = highestValue + graphPadding;


    // Get values for the Y axis
      const highestValueOnYAxis =
        ['tmax_C', 'tmin_C'].includes(dataToShow) ? Math.ceil(highestValue) : roundUpToNearest10(highestValue);
      const midPointOnYAxis = highestValueOnYAxis / 2;
      const lowestValueOnYAxis =
        ['tmax_C', 'tmin_C'].includes(dataToShow) ? Math.floor(lowestValue) : roundDownToNearest10(lowestValue);

      const yAxis = [
        { class: 'highest-point', value: highestValueOnYAxis },
        { class: 'mid-point', value: midPointOnYAxis },
        { class: 'lowest-point', value: lowestValueOnYAxis }
      ];
      if (lowestValue < 0) {
        yAxis.splice(2, 0, {class: 'zero', value: 0});
      }

      
    // Get Y axis label  
      let yAxisLabel = '';
      switch (dataToShow) {
        case 'tmax_C':
          yAxisLabel = 'Maximum temperature (in degrees Celsius)';
          break;

        case 'tmin_C':
          yAxisLabel = 'Minimum temperature (in degrees Celsius)';
          break;

        case 'rain_mm':
          yAxisLabel = 'Rain fall (in millimeters)';
          break;

        default:
          yAxisLabel = 'Sunshine hours';
          break;
      }
      

    // Put out the graph
      return (
        <div id="graph-container">

          <div id="graph-wrapper">
            <p className="axis y"><span dangerouslySetInnerHTML={{ __html: i18n.putOut(yAxisLabel) }} /></p>
            <ul id="y-axis">
              { yAxis.map((v, i) => {
                return (
                  <li
                    key={ i }
                    className={ v.class }
                    style={{ bottom: `${ getPercentage(v.value, highestValueOnYAxis, lowestValueOnYAxis, true) }%` }}>
                    <span>{ v.value }</span>
                  </li>
                );
              })}
            </ul>

            <ul id="graph" ref={el => (this.graph = el)}>
              { filteredData.map((v, k) => {
                const liWidth = 100 / filteredData.length;
                const month = moment(v.date, dateFormat).month();

                return (
                  <li
                    key={ k }
                    className={ cl({ 'first-month': month === 0 }) }
                    style={{ width: `${ liWidth }%`, left: `${ liWidth * k }%` }}>

                    { month === 0 ?
                      <p className={ cl('year', { 'vertical': noOfYears >= 10, 'hidden': noOfYears > 20 }) }>
                        { moment(v.date, dateFormat).year() }
                      </p>
                    : null }

                    <GraphPoints
                      id={ k }
                      data={ filteredData }
                      dataToShow={ dataToShow }
                      date={ v.date }
                      value={ v.value }
                      highestValue={ highestValueOnYAxis }
                      lowestValue={ lowestValueOnYAxis }
                      graphWidth={ graphWidth }
                      graphHeight={ graphHeight }
                    />

                  </li>
                );
              })}
            </ul>
          </div>

          <p className="axis x">
            { i18n.putOut( noOfYears > 1 ? 'Years' : 'Months') }
            <small>{ moment(startDate).format(dateFormat) } - { moment(endDate).format(dateFormat) }</small>
          </p>

        </div>
      );
  }
}


export default GraphWrapper;
