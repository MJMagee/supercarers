import moment from 'moment';


// Functions
  const getPercentage = (value, total, lowestValue) => {

    // If the lowest value is under 0, adjust the value and the total to make sure the graph is tall enough
    // to show numbers below zero. Adjusting the numbers to be that much bigger, effectively makes the negative
    // number 0 on the scale
      if (lowestValue < 0) {
        value += Math.abs(lowestValue);
        total += Math.abs(lowestValue);
      }

    return value / total * 100;
  };


  const getRealValue = (percentage, total) => {
    return (percentage / 100 * total);
  };


  const roundUpToNearest10 = (num) => {
    return Math.ceil(num / 10) * 10;
  };


  const roundDownToNearest10 = (num) => {
    return Math.floor(num / 10) * 10;
  };


  export { getPercentage, getRealValue, roundUpToNearest10, roundDownToNearest10 };

