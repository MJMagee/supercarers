import React, { Component } from 'react';
import cl from 'classnames';

class Button extends Component {

  render() {
    const { el = 'p', className = '', onClick, text } = this.props;

    return React.createElement(
      el,
      {
        className: cl('cta', className)
      },
      <button onClick={ onClick }>
        <span className="text">{ text }</span>
      </button>
    );
  }
}

export default Button;