import React, { Component } from 'react';
import i18n from 'i18n';

import { ErrorMessage } from 'components/form';


class FieldWrapper extends Component {

  render() {
    const { id, label, errors = {}, children, dtClass = '', ...props } = this.props;

    return [
        id in errors ?
          <ErrorMessage key="error">{ i18n.putOut(errors[id]) }</ErrorMessage>
        : null,

        <dt key="dt" className={ dtClass }>
          <label htmlFor={ id }>{ i18n.putOut(label) }</label>
        </dt>,

        <dd key="dd" {...props}>
          { children }
        </dd>
    ];
  }
}

export default FieldWrapper;