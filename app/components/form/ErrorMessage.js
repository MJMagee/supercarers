import React, { Component } from 'react';

class ErrorMessage extends Component {
  render() {
    const { el = 'dt', children } = this.props;

    return React.createElement(
      el,
      {
        className: 'error'
      },
      children
    );
  }
}

export default ErrorMessage;
