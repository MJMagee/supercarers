import React, { Component } from 'react';
import { RadioButton } from 'components/form';

class RadioButtonGroup extends Component {

  render() {
    const {
      name,
      options = [],
      selectedOption = null,
      onClick
    } = this.props;

    return (
      <ul className="radio-group">
        {
          options.map((option, i) => {
            return (
              <li key={ i }>
                <RadioButton
                  id={ option.id ? option.id : `${name}-${i}` }
                  name={ name }
                  value={ option.value }
                  label={ option.label }
                  selectedOption={ selectedOption }
                  onClick={ onClick }
                />
              </li>
            );
          })
        }
      </ul>
    );

  }
}

export default RadioButtonGroup;
