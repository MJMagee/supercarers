import React, { Component } from 'react';
import i18n from 'i18n';

class RadioButton extends Component {

  render() {
    const {
      id,
      name,
      value,
      label,
      selectedOption,
      onClick
    } = this.props;

    return (
      <span onClick={ (e) => { e.preventDefault(); onClick(value); } }>
        <input type="radio" id={ id } name={ name } value={ value } checked={ selectedOption === value } />
        <span className="input radio" />
        <label htmlFor={ id } dangerouslySetInnerHTML={{ __html: i18n.putOut(label) }} />
      </span>
    );

  }
}

export default RadioButton;
