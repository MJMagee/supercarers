import React, { Component } from 'react';
import moment from 'moment';
import i18n from 'i18n';


const dateFormat = 'MM/YYYY';
const expectedDateFormats = [
  'M/Y',
  'MM/YY',
  'M/YYYY',
  'MM/YYYY'
];
const getDate = (date) => {
  if (date === '' || date === null) { return null; }

  date = moment.utc(date, expectedDateFormats, true).toDate();
  if (date && date.constructor.name === 'Date' && !isNaN(Date.parse(date))) { return date; }

  return null;
};


export class DateField extends Component {

  constructor({ value }) {
    super();

    value = getDate(value);
    this.state = {
      rawValue: value || '',
      focused: false
    };
  }

  componentWillReceiveProps(nextProps) {
    const { focused } = this.state;
    const nextDate = getDate(nextProps.value);

    if (nextDate && !focused) {
      this.setState({ rawValue: nextDate });
    }
  }

  render() {
    const { onChange, onBlur, ...props } = this.props;
    const { rawValue } = this.state;

    return (
      <input
        type="text"
        className="date"
        onKeyPress={ (e) => {
          const key = e.key;
          const whiteList = [
            'Decimal', '/',
            'ArrowUp', 'ArrowRight', 'ArrowDown', 'ArrowLeft',
            'Clear', 'Backspace', 'Delete', 'Insert', 'Paste'
          ];

          if (!isNaN(parseFloat(key) && isFinite(key)) || whiteList.includes(key)) { return; }

          e.preventDefault();
          return false;
        }}
        onChange={ (e) => {
          const value = e.target.value;
          const isDate = getDate(value);
          this.setState({ rawValue: value});
          onChange(value, isDate);
        }}
        onBlur={ (e) => {
          const value = e.target.value;
          const isDate = getDate(value);

          onBlur(value, isDate);
        }}
        value={ rawValue }
        placeholder={ i18n.putOut('MM / YYYY') }
        { ...props }
      />
    );
  }
}

export default DateField;
