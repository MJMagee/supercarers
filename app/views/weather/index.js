import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import LoadingIndicator from 'components/LoadingIndicator';
import Form from './Form';
import Graph from 'components/graph';
import data from 'data/heathrow-info.json';


const mapStateToProps = (state) => {
  return {
    weather: state.weather
  }
};

let loadingIndicatorTimeout;
class Weather extends Component {

  constructor() {
    super();

    this.state = {
      isLoading: false
    };

    this.hideGraph = this.hideGraph.bind(this);
  }

  hideGraph(data) {
    this.setState({ isLoading: true });
    
    loadingIndicatorTimeout = setInterval(() => {
      const requiredFields = [
        { id: 'startDate' },
        { id: 'endDate' },
        { id: 'dataToShow' }
      ];
      let updated = 0;

      _.each(data, (key) => {
        if (requiredFields[key] === this.props.weather[key]) { updated++; }
      });

      if (updated === requiredFields.length) {
        clearInterval(loadingIndicatorTimeout);
        this.setState({ isLoading: false });
      }

    }, 250);
  }
  

  render() {
    const { weather } = this.props;
    const { isLoading } = this.state;

    return (
      <div id="weather">
        { isLoading ? <LoadingIndicator /> : null }
        { !isLoading ? <Graph data={ data } {...weather} /> : null }
        <Form hideGraph={ this.hideGraph } {...weather} />
      </div>
    );
  }

}

export default connect(mapStateToProps)(Weather);
