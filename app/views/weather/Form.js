import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import i18n from 'i18n';

import { weatherState as mapStateToProps, weatherDispatch as mapDispatchToProps } from 'maps';

import { FieldWrapper, RadioButtonGroup, Button } from 'components/form';
import DateFieldWrapper from './DateFieldWrapper';
import dataOptions from './dataOptions';


class Form extends Component {

  constructor() {
    super();

    this.state = {
      startDate: null,
      endDate: null,
      dataToShow: null,
      errors: {}
    };

    this.addError = this.addError.bind(this);
    this.removeError = this.removeError.bind(this);
    this.updateState = this.updateState.bind(this);
  }

  addError(key, value) {
    const { errors } = this.state;

    errors[key] = value;
    this.setState({ errors });
  }

  removeError(key) {
    const { errors } = this.state;
    if (errors[key]) {
      delete errors[key];
    }

    this.setState({ errors });
  }

  updateState(key, value) {
    this.setState({ [key]: value });
  }
  

  render() {
    const { changeDate, changeDataType, hideGraph } = this.props;
    let { startDate, endDate, dataToShow, errors } = this.state;

    const requiredFields = [
      {
        id: 'startDate',
        value: startDate,
        error: 'Please enter a start date'
      },
      {
        id: 'endDate',
        value: endDate,
        error: 'Please enter an end date'
      },
      {
        id: 'dataToShow',
        value: dataToShow,
        error: 'Please pick a data type'
      }
    ];

    return (
      <form>

        <dl className="clearfix">
          <dt className="overarcing-label">
            { i18n.putOut('What date range would you like to see data for?') }
          </dt>
          <dd className="small-wrapper">
            <DateFieldWrapper
              id="startDate"
              label="Start date"
              value={ startDate }
              errors={ errors }
              addError={ this.addError }
              removeError={ this.removeError }
              changeDate={ this.updateState }
            />
            <DateFieldWrapper
              id="endDate"
              label="End date"
              value={ endDate }
              errors={ errors }
              addError={ this.addError }
              removeError={ this.removeError }
              changeDate={ this.updateState }
            />
          </dd>

          <FieldWrapper
            id="dataToShow"
            label="What data would you like to see?"
            errors={ errors }
            dtClass="radio-group-label"
          >
            <RadioButtonGroup
              name="dataToShow"
              options={ dataOptions }
              selectedOption={ dataToShow }
              onClick={ (value) => {
                this.updateState('dataToShow', value);
                this.removeError('dataToShow');
              }}
            />
          </FieldWrapper>

        </dl>

        <Button
          text={ i18n.putOut('Show data') }
          onClick={ (e) => {
            e.preventDefault();
            e.stopPropagation();

            // Show errors for any required fields we don't have
              let missingFields = 0;
              requiredFields.map((requiredField) => {

                if (!requiredField.value) {
                  this.addError(requiredField.id, requiredField.error);
                  missingFields++;
                }
              });

              if (Object.keys(errors).length === 0 && missingFields === 0) {

                
                // Ensure the earliest date is first
                  if (moment(endDate, 'MM/YYYY').isBefore(moment(startDate, 'MM/YYYY'))) {
                    const newStartDate = endDate;
                    const newEndDate = startDate;
                    
                    startDate = newStartDate;
                    endDate = newEndDate;

                    this.updateState('startDate', startDate);
                    this.updateState('endDate', endDate);
                  }
                
                
                // Hide the graph
                  hideGraph({ startDate, endDate, dataToShow });


                // Update state
                  changeDate('start', startDate);
                  changeDate('end', endDate);
                  changeDataType(dataToShow);
              }

          }}
        />

      </form>
    );
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(Form);
