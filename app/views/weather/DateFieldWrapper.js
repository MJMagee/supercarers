import React, { Component } from 'react';
import moment from 'moment';
import i18n from 'i18n';

import { FieldWrapper, DateField } from 'components/form';


// Check if the given date is within the range we have for the data
  const dateOutsideRange = (date) => {

    const format = 'MM/YYYY';
    const earliestDate = moment('01/1958', format);
    const latestDate = moment('12/2014', format);
    
    if (moment(date, format).isBetween(earliestDate, latestDate, null, '[]')) { return false; }
    if (moment(date, format).isBefore(earliestDate)) { return i18n.putOut('Too early'); }
    return i18n.putOut('Too late');
  };


class DateFieldWrapper extends Component {

  render() {

    const { id, label, value, errors, addError, removeError, changeDate } = this.props;

    return (
      <dl className="small">
        <FieldWrapper id={ id } label={ label } errors={ errors }>
          <DateField
            id={ id }
            value={ value }
            onChange={(value, validDate) => {
              changeDate(id, value);
              if (validDate) { removeError(id); }
            }}
            onBlur={(value, validDate) => {
              if (value) {
                if (!validDate) { addError(id, 'Invalid date'); }
                else {
                  const isOutsideRange = dateOutsideRange(value);
                  if (isOutsideRange) { addError(id, isOutsideRange); }
                }
              }
            }}
          />
        </FieldWrapper>
      </dl>
    );
  }
}

export default DateFieldWrapper;