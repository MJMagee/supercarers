const dataOptions = [
  {
    value: 'tmax_C',
    label: 'Maximum temperature (in degrees Celsius)'
  },
  {
    value: 'tmin_C',
    label: 'Minimum temperature (in degrees Celsius)'
  },
  {
    value: 'rain_mm',
    label: 'Rain fall (in millimeters)'
  },
  {
    value: 'sunshine_hours',
    label: 'Sunshine hours'
  }
];

export default dataOptions;