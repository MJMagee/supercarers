// The text is bundled together from each different module into text
  import enGB from './text/enGB'

// TODO: this needs to actually work out which language it should return
// But for now, it can just return English
  export default {
      i18n: enGB
  };