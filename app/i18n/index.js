import config from './config';

export default {

    //  If a translation is available, use it, otherwise default to the string provided
      putOut: function(s) {
          if (typeof s!== 'undefined' && typeof config.i18n !== 'undefined' && config.i18n[s]) {
            let subs=config.i18n[s];

            // Basic mark-up
              subs=subs.replace(/\[span\]([^/]+)\[\/span\]/g, '<span>$1</span>');


            // Take any additional arguments we may have just set up and substitute them
            // This will find [$1], [$2], etc., and replace them with additional arguments sent to putOut()
            // in the order they are passed in
              for (let i=1; i<arguments.length; i++) {
                subs=subs.replace(new RegExp('\\[\\$'+i+'\\]', 'g'), arguments[i]);
              }

            return subs;
          }

          return s;
      },


    // Format numbers
      formatNumbers: function(n, noDecimal) {
        if (typeof n === 'number' && (typeof noDecimal === 'undefined' || noDecimal === false)) {
          n = Math.abs(n).toFixed(2);
        }

        return n.toString().replace(/./g, function(c, i, a) {
            return i && c!=='.' && !((a.length - i) % 3) ? config.i18n.thousandsSeparator+c : c;
        });
      }

  };
