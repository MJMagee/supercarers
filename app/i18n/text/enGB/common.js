export default {

  // Number formatting
      'thousandsSeparator': ',',

  // Common text
      'Loading': 'Loading...',
      'MM / YYYY': 'MM / YYYY'

};