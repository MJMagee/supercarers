export default {

  'What date range would you like to see data for?': 'What date range would you like to see data for?',
  'Start date': 'Start date:',
  'Please enter a start date': 'What date would you like to see the data from? Please enter a month and year.',
  'End date': 'End date:',
  'Please enter an end date': 'What date would you like to see the data to? Please enter a month and year.',
  'Invalid date': 'Oops! That doesn\'t look like a valid date. Please enter a month and year, e.g. 07/1985.',
  'Too early': 'Sorry! Our records only go back as far as January 1958.',
  'Too late': 'Sorry! Our records haven\'t yet been updated past December 2014.',

  'What data would you like to see?': 'What data would you like to see?',
  'Maximum temperature (in degrees Celsius)': 'Maximum temperature [span](in degrees Celsius)[/span]',
  'Minimum temperature (in degrees Celsius)': 'Minimum temperature [span](in degrees Celsius)[/span]',
  'Rain fall (in millimeters)': 'Rain fall [span](in millimeters)[/span]',
  'Sunshine hours': 'Sunshine hours',

  'Max. temperature:': 'Max. temperature:',
  'Min. temperature:': 'Min. temperature:',
  'Rain fall:': 'Rain fall:',
  'Sunshine hours:': 'Sunshine hours:',
  'mm': 'mm',
  'c': 'c',

  'Please pick a data type': 'What data are you interested in seeing? Please pick from the list.',


  'Years': 'Years',
  'Months': 'Months',

  'Show data': 'Show data',

};