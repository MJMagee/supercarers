// Export all of the modules as a single object to make interacting with any language easier
  import _ from 'lodash';

  import common from './common';
  import weather from './weather';

  const text = _.extend({},
    common,
    weather
  );


  export default text;
