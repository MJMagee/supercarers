export default (state = { startDate: null, endDate: null, dataToShow: null }, action) => {
  switch (action.type) {
    case 'CHANGE_START_DATE':
      return {
        ...state,
        startDate: action.startDate
      };

    case 'CHANGE_END_DATE':
      return {
        ...state,
        endDate: action.endDate
      };

    case 'CHANGE_DATA_TYPE':
      return {
        ...state,
        dataToShow: action.dataToShow
      };


    default:
      return state;
  }
};