import { weatherChangeDate, weatherChangeDataType } from 'actions';

export const weatherState = (state) => {
  return {
    weather: state.weather
  }
};

export const weatherDispatch = (dispatch) => {
  return {
    changeDate: (type, date) => { dispatch(weatherChangeDate(type, date)); },
    changeDataType: data => dispatch(weatherChangeDataType(data))
  }
};