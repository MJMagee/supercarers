module.exports = function(grunt) {

  const app = 'app';
  const appAssets = 'assets';
  const publicAssets = 'public-assets';

  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
  grunt.initConfig({


    // Change SCSS files to CSS files
      sass: {
        build: {
          options: {
            style: 'compressed',
            sourcemap: 'none',
            noCache: true
          },
          files: [{
            expand: true,
            cwd: appAssets,
            src: ['css/**/*.scss'],
            dest: publicAssets,
            ext: '.css'
          }]
        }
      },


    // Add vendor prefixes
       postcss: {
          options: {
            map: {
              inline: false,
              annotation: publicAssets + '/css'
            },
            processors: [
              require('autoprefixer')({browsers: 'last 3 versions'}) // add vendor prefixes
            ]
          },
          dist: {
            expand: true,
            cwd: publicAssets,
            src: ['css/**/*.css'],
            dest: publicAssets
          }
        },


    // Handle JS
      shell: {
         dev: {
           command: 'webpack --display-error-details'
         }
      },


    // Copy across files
      copy: {

        images: {
          files: [{
            expand: true,
            cwd: appAssets,
            src: ['img/**/*'],
            dest: publicAssets,
            timestamp: true
          }]
        },
        data: {
          files: [{
            expand: true,
            cwd: appAssets,
            src: ['data/**/*'],
            dest: publicAssets,
            timestamp: true
          }]
        }
      },


    // Notify
      notify: {
        complete: {
          options: {
            title: 'Done!',
            message: 'Grunt has built ALL THE THINGS!'
          }
        }
      },


    // Set up watchers for development - webpack has its own watcher for js
      watch: {
        css: {
          files: [appAssets + '/css/**/*.scss'],
          tasks: ['sass', 'postcss', 'notify:complete']
        },
        js: {
          files: [app + '/**/*', 'webpack.config.js'],
          tasks: ['shell:dev', 'notify:complete']
        }
      }
  });


  grunt.registerTask('build', [
    'sass',
    'postcss',
    'newer:copy',
    'shell:dev',
    'notify:complete'
  ]);

};
